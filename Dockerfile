FROM kw90/ros-kinetic-mb-movebase:latest

WORKDIR /ros_ws/src

RUN git clone -b kinetic https://github.com/skasperski/navigation_2d.git

WORKDIR /ros_ws

SHELL ["/bin/bash", "-c"]# Change to bash shell for ros stuff

RUN rosdep install --from-paths src -i -y --rosdistro kinetic

RUN source /opt/ros/kinetic/setup.bash && \
    catkin init && \
    catkin config --install && \
    catkin_make -j6

COPY launch-files /launch-files
COPY run-shells /run-shells
COPY config /config

ENTRYPOINT ["/root/entrypoint.sh"]

ENV ROS_MASTER_URI "http://ros-master:11311"

CMD ["bash"]

