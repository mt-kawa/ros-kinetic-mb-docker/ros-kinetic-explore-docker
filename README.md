# 2D-Nav: Modules for Navigation
Set of Libraries for Exploration, Reactive Obstacle Avoidance, Graph Based SLAM
for cooperative mapping, etc. Credits to Sebastian Kasperski for this great
library to be found [here](http://wiki.ros.org/nav2d).

## Getting Image

To get the image from RepoHub

```zsh
docker pull repohub.enterpriselab.ch:5002/kawa/ros-kinetic-explore:latest
```

```zsh
git clone
https://gitlab.enterpriselab.ch/mt-kawa/ros-kinetic-mb-docker/ros-kinetic-explore.git
cd ros-kinetic-explore
docker build -t repohub.enterpriselab.ch:5002/kawa/ros-kinetic-explore:latest .
```

## Running Exploration

### Requirements
+ driver node
+ odometry

To run exploration run the following command

```zsh
docker run -it --name ros-explore --network $(docker network ls --filter="NAME=roscomm" --quiet) repohub.enterpriselab.ch:5002/kawa/ros-kinetic-explore:latest bash
```

Wait for the container bash and run

```bash
# /run-shells/run-exploration.sh
```

Check that all nodes get started correctly and start a new terminal and run

```zsh
$ docker exec -it ros-explore bash
# source devel/setup.bash
# rosservice call /StartExploration
```

The base will now autonomously explore the area and build the map.

Once the exploration has finished, the following should be printed to the log of
the exploration node

```log
[ INFO] [1562971682.401099590, 895.462000000]: Exploration has finished.
```
